FROM debian:buster

# Required for package installation
COPY inn.conf /etc/news/inn.conf

RUN apt update && \
apt install -o Dpkg::Options::='--force-confold' -y inn2 msmtp && \
rm -rf /var/lib/apt/lists/*

CMD ["/usr/lib/news/bin/innd", "-d"]
